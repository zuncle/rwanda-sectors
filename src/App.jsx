import React, {useState, useEffect, useRef} from 'react';
import {WebMercatorViewport} from 'react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import "./App.css";

import ReactMapGL from 'react-map-gl';
//import {StaticMap} from 'react-map-gl';

import getStyle from "./style"

function Map() {

    const map = useRef(null);
    const [hoveredSectorCode, setHoveredSectorCode] = useState(null)
    const [minZoom, setMinZoom] = useState(5)

    //xMin,yMin 28.8617,-2.84023 : xMax,yMax 30.8997,-1.04717
    const minLongitude = 28.8616
    const maxLongitude = 30.8997
    const maxLatitude = -1.04717
    const minLatitude = -2.84023

    // southwest coordinates, northeast coordinates
    const bounds = [[minLongitude, maxLatitude], [maxLongitude, minLatitude]]

    const [viewport, setViewport] = useState(null);

    useEffect(() => {
        //if ( hoveredSectorCode != null )
        //    console.log(hoveredSectorCode)
        //else
        //    console.log("hovered sector is null")
    }, [hoveredSectorCode]);

    function onViewportChange(viewport){
        console.log(viewport)

        if ( viewport.longitude < minLongitude ) {
            viewport.longitude = minLongitude;
        }
        if ( viewport.longitude > maxLongitude ) {
            viewport.longitude = maxLongitude;
        }
        if ( viewport.latitude < minLatitude ) {
            viewport.latitude = minLatitude;
        }
        if ( viewport.latitude > maxLatitude ) {
            viewport.latitude = maxLatitude;
        }

        setViewport(viewport)
    }

    function selectFeature(selected, id){
        if ( id == null ) return
        map.current.getMap().setFeatureState({
            source: "sectors",
            sourceLayer: "rwandasectors",
            id: id
        }, {
            hover: selected
        })
    }

    function onHover(event){
        const features = map.current.queryRenderedFeatures(event.point, { layers: ['sectors'] });
        if ( features != null && features.length > 0 ) {
            const feature = features[0]

            if ( feature.properties.ADM3_PCODE !== hoveredSectorCode){
                selectFeature(false, hoveredSectorCode)
            }

            selectFeature(true, feature.properties.ADM3_PCODE)
            //console.log(feature.properties)
            setHoveredSectorCode(feature.properties.ADM3_PCODE)
        }
        else {
            selectFeature(false, hoveredSectorCode)
            setHoveredSectorCode(null)
        }
    }

    function onClick(event) {
        const features = map.current.queryRenderedFeatures(event.point, { layers: ['sectors'] });

        if (features != null && features.length > 0) {
            const properties = features[0].properties;
            //console.log(properties);
        }
    }

    function onLoad(){
        const initViewport = new WebMercatorViewport({
            width: viewport.width,
            height: viewport.height
        }).fitBounds(bounds);
        setViewport(initViewport)
        setMinZoom(initViewport.zoom)
        //console.log(initViewport)
    }

    return (
        <ReactMapGL
            ref={map}

            {...viewport}
            attributionControl={false}
            onViewportChange={onViewportChange}
            mapboxApiAccessToken={"no-token"}

            mapStyle={getStyle()}
            touchRotate={false}
            dragRotate={false}

            minZoom = {minZoom}
            maxZoom={12}

            width="100vw"
            height="100vh"

            onLoad={(e) => onLoad(e)}
            onHover={(e) => onHover(e)}
            onClick={(e) => onClick(e)}

        >
        </ReactMapGL>
    );
}


function App() {
  return (
      <Map/>
  );
}

export default App;
