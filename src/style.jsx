export default function getStyle() {

    //const baseUrl = "http://localhost:9001"
    const baseUrl = "https://maptiles.pixime.fr"
    const stageUrl = "https://maptiles.stage.pixime.fr"

    return {
        "version": 8,
        "name": "hazard-per-sectors",
        "id": "hazard-per-sectors",
        "metadata": {
            "mapbox:autocomposite": false,
            "mapbox:type": "template"
        },

        "center": [
            0,
            0
        ],

        "zoom": 1,
        "bearing": 0,
        "pitch": 0,

        "glyphs": baseUrl + "/fonts/{fontstack}/{range}.pbf",
        "sprite": baseUrl + "/sprites/osm-liberty",

        "sources": {
            "sectors": {
                "type": "vector",
                "tiles": [
                    baseUrl + "/tiles/rwanda-sectors/{z}/{x}/{y}"
                ],
                "promoteId": "ADM3_PCODE"
            },
            "water-bodies": {
                "type": "vector",
                "tiles": [
                    baseUrl + "/tiles/rwanda-water-bodies/{z}/{x}/{y}"
                ]
            },
            "country": {
                "type": "vector",
                "tiles": [
                    baseUrl + "/tiles/rwanda-country/{z}/{x}/{y}"
                ]
            },
            "temp": {
                "type": "vector",
                "tiles": [
                    stageUrl + "/tiles/050000w3/{z}/{x}/{y}"
                ]
            },
            "controids": {
                "type": "vector",
                "tiles": [
                    baseUrl + "/tiles/rwanda-controids/{z}/{x}/{y}"
                ]
            }
        },

        "layers": [
            {
                "id": "background",
                "type": "background",
                "paint": {
                    "background-color": "#E6E6E6"
                }
            },
            {
                "id": "water-bodies",
                "interactive": false,
                "type": "fill",
                "source": "water-bodies",
                "source-layer": "rwandawaterbodies",
                "layout": {
                    "visibility": "visible"
                },
                "paint": {
                    "fill-color": "#D4DBE2"
                }
            },
            {
                "id": "country",
                "type": "line",
                "interactive": false,
                "source": "country",
                "source-layer": "rwandacountry",
                "layout": {
                    "visibility": "visible"
                },
                "paint": {
                    "line-color": "#FFFFFF",
                    "line-width": 6,
                }
            },
            {
                "id": "sectors",
                "type": "fill",
                "interactive": true,
                "source": "sectors",
                "source-layer": "rwandasectors",
                "layout": {
                    "visibility": "visible"
                },
                "paint": {
                    "fill-color": "#6666CD",
                    "fill-opacity": [
                        'case',
                        ['boolean', ['feature-state', 'hover'], false],
                        0.65,
                        1
                    ],
                    "fill-outline-color": "#FFFFFF"
                }
            },
            {
                "id": "temp",
                "type": "fill",
                "interactive": true,
                "source": "temp",
                "source-layer": "050000W3",
                "layout": {
                    "visibility": "visible"
                },
                "paint": {
                    "fill-opacity": 0.75,
                    'fill-color': [
                        'interpolate',
                        ['linear'],
                        ['get', 'DN'],
                        15.5,
                        '#FEF0D8',
                        17.5,
                        '#FECD85',
                        19.5,
                        '#FE8D52',
                        21.5,
                        '#E5492A',
                        23.5,
                        '#B50000',
                    ],
                }
            },
            {
                "id": "sector-name",
                "type": "symbol",
                "interactive": false,
                "source": "controids",
                "source-layer": "rwandacontroids",
                "layout": {
                    "text-field": "{ADM3_EN}\n",
                    "text-size": 12,
                    "text-allow-overlap": true,
                    "text-ignore-placement": true,
                    "text-font": [
                        "Roboto Regular"
                    ],
                },
                "paint": {
                    "text-color": ["case",
                        ["boolean", ["feature-state", "hover"], false],
                        'rgba(255,0,0,0.75)',
                        'rgba(0,0,0,0.75)'
                    ],
                    "text-halo-color": ["case",
                        ["boolean", ["feature-state", "hover"], false],
                        'rgba(255,255,0,0.75)',
                        'rgba(255,255,255,0.75)'
                    ],
                    "text-halo-width": 1.25,
                    "text-halo-blur": 0,
                }
            },
            {
                "id": "sector-name",
                "type": "symbol",
                "interactive": false,
                "source": "sectors",
                "source-layer": "rwandasectors",
                "minzoom": 8,
                "layout": {
                    "text-field": "{ADM3_EN}\n",
                    "text-size": 12,
                    'symbol-placement': "point",
                    "text-font": [
                        "Roboto Regular"
                    ],
                },
                "paint": {
                    "text-color": ["case",
                        ["boolean", ["feature-state", "hover"], false],
                        'rgba(255,0,0,0.75)',
                        'rgba(0,0,0,0.75)'
                    ],
                    "text-halo-color": ["case",
                        ["boolean", ["feature-state", "hover"], false],
                        'rgba(255,255,0,0.75)',
                        'rgba(255,255,255,0.75)'
                    ],
                    "text-halo-width": 1.25,
                    "text-halo-blur": 0,
                }
            },
        ]
    }
};